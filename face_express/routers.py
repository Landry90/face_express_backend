from rest_framework.routers import DefaultRouter
from core.views import FaceImageViewSet

router = DefaultRouter()

router.register('face_image', FaceImageViewSet, basename = 'face_image')

#print(router.urls)

urlpatterns = router.urls