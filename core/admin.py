from django.contrib import admin
from core.models import FaceImage

# Register your models here.

class FaceImageAdmin(admin.ModelAdmin):
    list_display = ('face_image', 'label')
    list_filter = ('face_image', 'label')
    search_fields = ['face_image', 'label']


admin.site.register(FaceImage, FaceImageAdmin)