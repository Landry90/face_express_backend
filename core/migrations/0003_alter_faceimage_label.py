# Generated by Django 4.2.3 on 2023-07-08 07:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0002_alter_faceimage_face_image'),
    ]

    operations = [
        migrations.AlterField(
            model_name='faceimage',
            name='label',
            field=models.CharField(max_length=10, null=True),
        ),
    ]
