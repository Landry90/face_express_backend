from django.shortcuts import render, get_object_or_404
from rest_framework import status
from rest_framework.response import Response
from rest_framework import viewsets
from .serializers import FaceImageSerializer
from .models import FaceImage

# Create your views here.

class FaceImageViewSet(viewsets.ModelViewSet):
    queryset =  FaceImage.objects.all()
    serializer_class = FaceImageSerializer

    def create(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(serializer.data, status=status.HTTP_201_CREATED)

    
    def retrieve(self, request, pk=None):
        image = get_object_or_404(self.queryset, pk=pk)
        serializer = FaceImageSerializer(image)
        
        return Response(serializer.data, status=status.HTTP_200_OK)
    
    def update(self, request, pk=None):
        instance = self.get_object(pk=pk)
        serializer = self.get_serializer(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        return Response(serializer.data, status=status.HTTP_202_ACCEPTED)