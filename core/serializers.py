from rest_framework import serializers
from .models import FaceImage

class FaceImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = FaceImage
        fields = ('face_image', 'label')