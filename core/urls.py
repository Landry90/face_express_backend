'''
from django.conf import settings
from django.conf.urls.static import static
from django.urls import path
from .views import UploadImageView


urlpatterns = [
    # urls de l'application core
    path('upload/', UploadImageView.as_view(), name = 'upload-image'),
    
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
'''